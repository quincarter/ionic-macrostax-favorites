export declare type FavoriteType = 'Food' | 'Recipe';

export interface FoodConversion {
  __typename: 'Conversion';
  calories: string | number;
  carb_grams: string | number;
  fat_grams: string | number;
  id: number;
  is_default: boolean;
  name: string;
  protein_grams: string | number;
  weight_grams: string | number;
}

export interface FavoriteFood {
  __typename: FavoriteType;
  brand_name: string;
  conversion: FoodConversion;
  conversions: FoodConversion[];
  id: number;
  is_favorite: boolean;
  name: string;
  nix_photo_url: string;
  quantity: number;
}

interface PhotoUrls {
  __typename: 'PhotoUrls';
  large: string;
  small: string;
}

export interface FavoriteRecipe {
  __typename: FavoriteType;
  bulk_make_qty: number;
  child_recipes: FavoriteRecipe[];
  conversion: FoodConversion;
  conversions: FoodConversion[];
  cook_loss_factor: number;
  cooked_weight_grams: number;
  custom_serving_name: string;
  eat_quantity: number;
  id: number;
  intro_line: string;
  is_bulk: boolean;
  is_discrete: boolean;
  is_favorite: boolean;
  is_miscible: boolean;
  make_quantity: number;
  name: string;
  notes: string;
  photo_urls: PhotoUrls;
  prep: string;
  prep_time: number;
  quantity: number;
  table_time: number;
  user_id: number | string;
}

interface CustomFoodIngredients {
  carbohydrates: number;
  protein: number;
  fat: number;
  fiber: number;
  calories: number;
}

export interface CustomFood {
  id: number;
  ingredient_id: number;
  name: string;
  common_servings: string;
  common_serving_unit_name: string;
  common_serving_units_name: string;
  common_serving_quantity: number;
  measurement_size: string | number;
  measurement_size_unit_name: string | number;
  measurement_size_quantity: string | number;
  calories: number;
  carbohydrates: number;
  fat: number;
  fiber: number;
  protein: number;
  ingredient: CustomFoodIngredients;
  servings: number;
  serving_size: number;
  serving_step: number;
  units: string;
  food_id: string | number;
  recipe_id: string | number;
  root_recipe_id: string | number;
  is_viewable: string | number;
  is_custom: boolean;
  is_favorite: boolean;
  favoritable: boolean;
}
