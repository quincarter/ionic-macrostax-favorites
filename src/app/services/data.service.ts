import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { CustomFood, FavoriteFood, FavoriteRecipe } from './foods';
import { FAVORITE_FOODS } from '../../data/favorites';
import { CUSTOM_FOODS } from '../../data/customFoods';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private favoriteFoodsSubject: BehaviorSubject<FavoriteFood[]> = new BehaviorSubject<FavoriteFood[]>(null);
  private customFoodsSubject: BehaviorSubject<CustomFood[]> = new BehaviorSubject<CustomFood[]>(null);
  private favoriteRecipesSubject: BehaviorSubject<FavoriteRecipe[]> = new BehaviorSubject<FavoriteRecipe[]>(null);

  public readonly favoriteFoods$: Observable<FavoriteFood[]> = this.favoriteFoodsSubject.asObservable();
  public readonly customFoods$: Observable<CustomFood[]> = this.customFoodsSubject.asObservable();
  public readonly favoriteRecipes$: Observable<FavoriteRecipe[]> = this.favoriteRecipesSubject.asObservable();
  constructor() {}

  public getFavoriteFoods() {
    this.favoriteFoodsSubject.next(FAVORITE_FOODS.sort((a, b) => (a.name < b.name ? -1 : 0)));
  }

  public getCustomFoods() {
    this.customFoodsSubject.next(CUSTOM_FOODS.sort((a, b) => (a.name < b.name ? -1 : 0)));
  }
}
