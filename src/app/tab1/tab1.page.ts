import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { Subscription } from 'rxjs';
import { FavoriteFood } from '../services/foods';
import Fuse from 'fuse.js';
import FuseResult = Fuse.FuseResult;

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  private favoriteFoodsSubscription: Subscription;
  public favoriteFoods: any;
  private foods: FavoriteFood[];
  public searchTerm: string = null;
  public options = {
    // isCaseSensitive: false,
    // includeScore: false,
    // shouldSort: true,
    // includeMatches: false,
    // findAllMatches: false,
    // minMatchCharLength: 1,
    // location: 0,
    // threshold: 0.6,
    // distance: 100,
    // useExtendedSearch: false,
    // ignoreLocation: false,
    // ignoreFieldNorm: false,
    keys: ['name', 'brand_name'],
  };

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.getFavoriteFoodsSubscription();
    this.dataService.getFavoriteFoods();
  }

  private getFavoriteFoodsSubscription() {
    this.favoriteFoodsSubscription = this.dataService.favoriteFoods$.subscribe((data) => {
      this.favoriteFoods = data;
      this.foods = data;
    });
  }

  filterFoods() {
    const fuse = new Fuse(this.foods, this.options);
    if (this.searchTerm) {
      this.favoriteFoods = fuse.search(this.searchTerm);
    } else {
      this.favoriteFoods = this.foods;
    }
  }
}
