import { Component, OnInit } from '@angular/core';
import { DataService } from '../services/data.service';
import { CustomFood } from '../services/foods';
import { Subscription } from 'rxjs';
import Fuse from 'fuse.js';
import FuseResult = Fuse.FuseResult;

@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss'],
})
export class Tab2Page implements OnInit {
  public customFoods: any;
  private foods: CustomFood[];
  public searchTerm: string;

  public options = {
    // isCaseSensitive: false,
    // includeScore: false,
    // shouldSort: true,
    // includeMatches: false,
    // findAllMatches: false,
    // minMatchCharLength: 1,
    // location: 0,
    // threshold: 0.6,
    // distance: 100,
    // useExtendedSearch: false,
    // ignoreLocation: false,
    // ignoreFieldNorm: false,
    keys: ['name', 'brand_name'],
  };

  private customFoodsSubscription: Subscription;

  constructor(private dataService: DataService) {}

  ngOnInit() {
    this.getCustomFoodsSubscription();
    this.dataService.getCustomFoods();
  }

  private getCustomFoodsSubscription() {
    this.customFoodsSubscription = this.dataService.customFoods$.subscribe((data) => {
      this.customFoods = data;
      this.foods = data;
    });
  }

  filterFoods() {
    const fuse = new Fuse(this.foods, this.options);
    if (this.searchTerm) {
      this.customFoods = fuse.search(this.searchTerm);
    } else {
      this.customFoods = this.foods;
    }
  }
}
